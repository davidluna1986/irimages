#include "thermalimage.h"

ThermalImage::ThermalImage(QObject *parent) :
    QObject(parent)
{
    MinTemp=1500;
    MaxTemp=0;
}

void ThermalImage::LoadCSVImage(QString pathimage)
{
    QString DataCSV;
    QStringList ListDataRowCSV;
    QStringList temp;

    float TempCSVtemp;
    float TempCSV[76800];
    unsigned int counterrow=0;



    CSVImage.setFileName(pathimage);
    CSVImage.open(QIODevice::ReadOnly);
    DataCSV=CSVImage.readAll();
    ListDataRowCSV=DataCSV.split("\n");

    for(int i=10;i<250;i++)
    {

        temp=ListDataRowCSV[i].split(",");
        temp.removeFirst();

        for(int j=0;j<320;j++)
        {

                QString datatemp=temp[j];
                TempCSVtemp=datatemp.toFloat();
                TempCSV[counterrow]=TempCSVtemp;

//                if(TempCSVtemp>MaxTemp)
//                {
//                    MaxTemp=TempCSVtemp;
//                    maxrow=i;
//                    maxcol=j;
//                }
//                if(TempCSVtemp<MinTemp)
//                {
//                    MinTemp=TempCSVtemp;
//                    minrow=i;
//                    mincol=j;
//                }
                counterrow++;
            }

    }

            MatData= cv::Mat(240, 320, CV_32FC1, TempCSV);
            MatData.copyTo(DataCopy);

     qDebug()<<DataCopy.at<float>(20,20);


            OutputImage= cv::Mat(240, 320, CV_8U, TempCSV);

//            mincenter.x=mincol;
//            mincenter.y=minrow;

//            maxcenter.x=maxcol;
//            maxcenter.y=maxrow;

            double mintemp;
            double maxtemp;

            cv::minMaxLoc(DataCopy, &mintemp, &maxtemp, &mincenter, &maxcenter);

            MinTemp=mintemp;
            MaxTemp=maxtemp;

            cv::normalize(MatData,OutputImage,0,255,cv::NORM_MINMAX, CV_8U);

            qDebug()<<DataCopy.at<float>(20,20);
            cv::applyColorMap(OutputImage,PseudocolorImage,cv::COLORMAP_JET);
            qDebug()<<OutputImage.at<unsigned char>(20,25);
            qDebug()<<DataCopy.at<float>(20,20);
            cv::circle(PseudocolorImage, mincenter, 5, cv::Scalar(0,0,0), 2, -1, 0);
            cv::circle(PseudocolorImage, maxcenter, 5, cv::Scalar(255,255,255), 2, -1, 0);

            cv:cvtColor(PseudocolorImage,PseudocolorImage,cv::COLOR_BGR2RGB);

//            cv::Rect roi(10, 20, 100, 50);
//            cv::Mat image_roi = im_color(roi);

            emit SendImage(PseudocolorImage,"thermal-color");
            emit SendImage(OutputImage,"thermal-gray");
            emit SendMaxTemp(QString::number(MaxTemp)+" C");
            emit SendMinTemp(QString::number(MinTemp)+" C");
            emit SendMDT(QString::number(MaxTemp-MinTemp)+" C");

}

void ThermalImage::SetImageROI(cv::Rect ROI)
{
    //qDebug()<<ROI.x<<ROI.y<<ROI.width<<ROI.height;

    if(ROI.x<=0)
        ROI.x=0;
    else if(ROI.x>=320)
        ROI.x=320;
    if(ROI.y<=0)
        ROI.y=0;
    else if(ROI.y>=240)
        ROI.y=240;
    if(((ROI.width+ROI.x)>=320)||ROI.width<=0)
        ROI.width=320-ROI.x;
    if(((ROI.height+ROI.y)>=240)||ROI.height<=0)
        ROI.height=240-ROI.y;


    //cv::Mat image_roi = PseudocolorImage(ROI);
    //cv::Mat image_roi2 =OutputImage(ROI);

    cv::Mat image_roi;
    cv::Mat image_roi2;
    cv::Mat image_roi3=DataCopy(ROI);

    PseudocolorImage.copyTo(image_roi);
    OutputImage.copyTo(image_roi2);


    double mintemp;
    double maxtemp;

    cv::rectangle(image_roi,ROI,cv::Scalar(255,255,255),2,8,0);
    cv::rectangle(image_roi2,ROI,cv::Scalar(255,255,255),2,8,0);

    cv::minMaxLoc(image_roi3, &mintemp, &maxtemp, &mincenter, &maxcenter);

    MinTemp=mintemp;
    MaxTemp=maxtemp;


    emit SendImage(image_roi,"thermal-color");
    emit SendImage(image_roi2,"thermal-gray");
    emit SendMaxTemp(QString::number(MaxTemp)+" C");
    emit SendMinTemp(QString::number(MinTemp)+" C");
    emit SendMDT(QString::number(MaxTemp-MinTemp)+" C");


}

void ThermalImage::PosInThermalIm(cv::Point MousePos)
{

    if(MousePos.x<0)
        MousePos.x=0;
    if(MousePos.x>320)
        MousePos.x=320;

    if(MousePos.y<0)
        MousePos.y=0;
    if(MousePos.y>240)
        MousePos.y=240;

    qDebug()<<MousePos.x<<MousePos.y;
        qDebug()<<DataCopy.at<float>(MousePos.y,MousePos.x);
        emit SendCurrentTemp(QString::number(DataCopy.at<float>(MousePos.y,MousePos.x),'f',3));

}

