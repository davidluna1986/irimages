#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    thermalimg=new ThermalImage();

    connections();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
   //qDebug()<<"pos "<<event->pos();
    //qDebug()<<"pos frame "<<ui->frame->pos().x()<<ui->frame->pos().y();
    if(event->button()==Qt::RightButton)
    {
        roi.x=event->pos().x()-ui->frame->pos().x();
        roi.y=event->pos().y()-ui->frame->pos().y()-10;
    }

}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    //qDebug()<<"pos "<<event->pos();
    if(event->button()==Qt::RightButton)
    {
        roi.width=event->pos().x()-roi.x-ui->frame->pos().x();
        roi.height=event->pos().y()-roi.y-ui->frame->pos().y()-10;
        emit SetImageROI(roi);
    }

}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{

    //qDebug()<<"pos "<<event->pos();
    cv::Point mousepos;
    mousepos.x=event->pos().x()-ui->frame->pos().x();
    mousepos.y=event->pos().y()-ui->frame->pos().y()-10;
    emit PosInThermalIm(mousepos);

}

void MainWindow::connections()
{
    this->connect(thermalimg,SIGNAL(SendImage(cv::Mat,QString)),this,SLOT(RcvImage(cv::Mat,QString)));
    this->connect(thermalimg,SIGNAL(SendMaxTemp(QString)),ui->lineEditMaxTemp,SLOT(setText(QString)));
    this->connect(thermalimg,SIGNAL(SendMDT(QString)),ui->lineEditMDT,SLOT(setText(QString)));
    this->connect(thermalimg,SIGNAL(SendMinTemp(QString)),ui->lineEditMinTemp,SLOT(setText(QString)));
    this->connect(this,SIGNAL(SetImageROI(cv::Rect)),thermalimg,SLOT(SetImageROI(cv::Rect)));
    this->connect(this,SIGNAL(PosInThermalIm(cv::Point)),thermalimg,SLOT(PosInThermalIm(cv::Point)));
    this->connect(thermalimg,SIGNAL(SendCurrentTemp(QString)),this,SLOT(SendCurrentTemp(QString)));
}

void MainWindow::RcvImage(cv::Mat img, QString type)
{

    if(type=="thermal-color")
    {
      QImage QtThermalImage( img.data, img.cols, img.rows, img.step, QImage::Format_RGB888 );
      QPixmap QtThermalPixmap=QPixmap::fromImage(QtThermalImage, Qt::AutoColor);
      ui->labelThermalImage->setPixmap(QtThermalPixmap.scaled(320,240,Qt::IgnoreAspectRatio));
    }
    else if(type=="thermal-gray")
    {
      QImage QtGrayImage( img.data, img.cols, img.rows, img.step, QImage::Format_Indexed8 );
      QPixmap QtGrayPixmap=QPixmap::fromImage(QtGrayImage, Qt::AutoColor);
      ui->labelGrayImage->setPixmap(QtGrayPixmap.scaled(320,240,Qt::IgnoreAspectRatio));
    }
}

void MainWindow::SendCurrentTemp(QString currenttemp)
{
    ui->lineEditCurrentTemp->setText(currenttemp);
}

void MainWindow::on_pushButtonLoadImg_clicked()
{
     QStringList pathselected=QFileDialog::getOpenFileNames(this, "Select a file to open...", QDir::homePath(),"*.csv");


     if(pathselected.length()>0)
     {
        ui->lineEditPathImage->setText(pathselected[0]);
        thermalimg->LoadCSVImage(pathselected[0]);
     }

}
