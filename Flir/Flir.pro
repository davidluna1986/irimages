#-------------------------------------------------
#
# Project created by QtCreator 2016-07-14T21:45:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Flir
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    thermalimage.cpp

HEADERS  += mainwindow.h \
    thermalimage.h

FORMS    += mainwindow.ui
INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib
LIBS+=-lopencv_videoio -lopencv_shape -lopencv_imgcodecs
LIBS+=-lopencv_core -lopencv_highgui -lopencv_imgproc #-lopencv_contrib
LIBS+=-lopencv_calib3d -lopencv_features2d -lopencv_flann -lopencv_ml
LIBS+=-lopencv_objdetect -lopencv_photo -lopencv_stitching -lopencv_superres
LIBS+=-lopencv_video -lopencv_videostab
