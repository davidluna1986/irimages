#ifndef THERMALIMAGE_H
#define THERMALIMAGE_H

#include <QFile>
#include <QDebug>
#include <QImage>
#include <QObject>

#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>


using namespace cv;

class ThermalImage:public QObject
{
        Q_OBJECT
public:
    explicit ThermalImage(QObject *parent = 0);

    void LoadCSVImage(QString pathimage);

    QFile CSVImage;
    float MaxTemp;
    float MinTemp;
    cv::Mat MatData;
    cv::Mat DataCopy;
    cv::Mat OutputImage;
    cv::Mat PseudocolorImage;
    cv::Point mincenter;
    cv::Point maxcenter;

signals:
    void SendImage(cv::Mat img,QString type);
    void SendMinTemp(QString mintemp);
    void SendMaxTemp(QString maxtemp);
    void SendCurrentTemp(QString CurrentTemp);
    void SendMDT(QString MDT);

public slots:
    void SetImageROI(cv::Rect);
    void PosInThermalIm(cv::Point);

};

#endif // THERMALIMAGE_H
