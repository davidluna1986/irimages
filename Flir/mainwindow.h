#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QMouseEvent>


#include "thermalimage.h"

using namespace cv;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
public slots:
    void RcvImage(cv::Mat img, QString type);
    void SendCurrentTemp(QString);
signals:
    void SetImageROI(cv::Rect);
    void PosInThermalIm(cv::Point);

private slots:
    void on_pushButtonLoadImg_clicked();


private:
    Ui::MainWindow *ui;
    void connections();
    ThermalImage *thermalimg;
    cv::Rect roi;
};

#endif // MAINWINDOW_H
