#include "nir.h"


double minScaleT = 0.0, maxScaleT = 1.0;

NIR::NIR(QObject *parent) :
    QObject(parent)
{
}

void NIR::NDVI(cv::Mat srcVis, cv::Mat srcNIR, cv::Mat *out, short option)
{
    out->release();
    if(option==0){
        out->create(srcVis.size(), CV_64FC1);
        *out = cv::Mat::zeros(srcVis.size(), CV_64FC1);
    }else{
        out->create(srcVis.size(), CV_32FC1);
        *out = cv::Mat::zeros(srcVis.size(), CV_32FC1);
    }
    int cols = srcVis.cols, rows=srcVis.rows;
    double outPix = 0.0;
    double pixelVis = 0.0;
    double pixelNIR = 0.0;
    double limitS = 1.0, limitI=-1.0;
    for(int r=0;r<rows;r++){
        for(int c=0;c<cols;c++){
            if(option==0){
                pixelVis = srcVis.at<double>(r, c);
                pixelNIR = srcNIR.at<double>(r, c);
            }else{
                pixelVis = srcVis.at<float>(r, c);
                pixelNIR = srcNIR.at<float>(r, c);
            }
            double pixVis = pixelVis*slopeVis + intercVis;
            double pixNIR = pixelNIR*slopeNIR + intercNIR;

            double sum1=pixVis+pixNIR;
            if(sum1==0.0){
                outPix = 0.0;
            }else{
                outPix = (pixNIR - pixVis)/sum1;
                if(outPix>limitS){
                    outPix = 1.0;
                }
                if(outPix<limitI){
                    outPix = -1.0;
                }
            }
            if(option==0){
                out->at<double>(r, c) = outPix;
            }else{
                out->at<float>(r, c) = (float)outPix;
            }
        }
    }
}

void NIR::scale_image(Mat src, Mat *dst, short option, short mode)
{

    dst->release();
    if(option==0){
        dst->create(src.size(), CV_64FC1);
        *dst = cv::Mat::zeros(src.size(), CV_64FC1);
    }else{
        dst->create(src.size(), CV_32FC1);
        *dst = cv::Mat::zeros(src.size(), CV_32FC1);
    }
    int cols = src.cols, rows=src.rows;
    uchar pixel=0;
    double pixelF = 0.0;
    double minV = 0.0, maxV = 255.0;
    if(mode!=0){
        cv::minMaxLoc(src, &minV, &maxV);
    }
   // std::cout<<"Minimo:"<<minV<<"/"<<"Maximo:"<<maxV<<std::endl;
    double inverseRange = 1 / (maxV - minV);
    for(int r=0;r<rows;r++){
        for(int c=0;c<cols;c++){
            pixel = src.at<uchar>(r,c);
            pixelF = inverseRange * (pixel - minV);
            if(option==0){
                dst->at<double>(r,c) = pixelF;
            }else{
                dst->at<float>(r,c) = (float)pixelF;
            }
        }
    }

}

void NIR::scale_image_byte(Mat src, Mat *dst, short mode)
{
    dst->release();
    dst->create(src.size(), CV_8UC1);
    *dst = cv::Mat::zeros(src.size(), CV_8UC1);
    double outPixF = 0.0;
    uchar outPix;
    int cols = src.cols, rows=src.rows;
    for(int r=0;r<rows;r++){
        for(int c=0;c<cols;c++){
            if(mode==0){
                outPixF = (src.at<double>(r,c)-minScaleT)/((maxScaleT - minScaleT)/255.0);
            }else{
                outPixF = (src.at<float>(r,c)-minScaleT)/((maxScaleT - minScaleT)/255.0);
            }
            if(outPixF<0){
                outPixF = 0.0;
            }
            outPix = (uchar)cvRound(outPixF);
            dst->at<uchar>(r,c)=outPix;
        }
    }
}

void NIR::GetNDVIColor(cv::Mat src,cv::Mat *dst)
{
    cv::Mat lutmapir(1,256, CV_8UC3);
    cv::Mat VisNorm, NirNorm;
    cv::Mat outNDVI, outNDVI_disp;
    std::vector<cv::Mat> tempoC;
    cv::Mat merf;

    dst->release();
    dst->create(src.size(), CV_32FC1);
    *dst = cv::Mat::zeros(src.size(), CV_32FC1);

    //Extraccion de planos visible(azul) y NIR

    cv::Mat planeblue;
    cv::Mat planeir;

    GetColorPlanes(src,&planeblue,&planeir);

    scale_image(planeir, &NirNorm, 1,0);
    scale_image(planeblue, &VisNorm, 1,0);

    //Calculo de indice
    NDVI(VisNorm, NirNorm, &outNDVI, 1);
    //Escalando imagen flotante a 8bits
    scale_image_byte(outNDVI, &outNDVI_disp, 1);

    //Aplicacion de LUT
    for(int j=0;j<256;j++){
        lutmapir.at<cv::Vec3b>(0,j)[0]=imLUT.at<cv::Vec3b>(0,j)[0];
        lutmapir.at<cv::Vec3b>(0,j)[1]=imLUT.at<cv::Vec3b>(0,j)[1];
        lutmapir.at<cv::Vec3b>(0,j)[2]=imLUT.at<cv::Vec3b>(0,j)[2];
    }
    tempoC.push_back(outNDVI_disp.clone());
    tempoC.push_back(outNDVI_disp.clone());
    tempoC.push_back(outNDVI_disp.clone());
    cv::merge(tempoC, merf);

    cv::LUT(merf, lutmapir,*dst);

    //Iniciano proceso de visualizacion, se redimensiona a un menor tamaño para su visualizacion
    if(src.cols>640){
        cv::resize(src, src, cv::Size(640,480));
        cv::resize(planeblue, planeblue, cv::Size(640,480));
        cv::resize(planeir, planeir, cv::Size(640,480));
        cv::resize(outNDVI_disp, outNDVI_disp, cv::Size(640,480));
        cv::resize(*dst, *dst, cv::Size(640,480));
    }
}

void NIR::GetColorPlanes(cv::Mat src, cv::Mat *out1, cv::Mat *out2)
{

    out1->release();
    out1->create(src.size(), CV_8UC1);
    *out1 = cv::Mat::zeros(src.size(), CV_8UC1);
    out2->release();
    out2->create(src.size(), CV_8UC1);
    *out2 = cv::Mat::zeros(src.size(), CV_8UC1);

    //Extraccion de planos visible(azul) y NIR
    std::vector<cv::Mat> planescolor;

    cv::split(src, planescolor);
    planescolor[0].copyTo(*out1);
    planescolor[2].copyTo(*out2);
}
