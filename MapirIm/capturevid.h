#ifndef CAPTUREVID_H
#define CAPTUREVID_H

#include <QThread>
#include <QDebug>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace cv;

class CaptureVid : public QThread
{
    Q_OBJECT
public:
    explicit CaptureVid(QObject *parent = 0);
    void run();
    void stop();

    cv::VideoCapture CaptureVideo;
    cv::VideoCapture CapVideoFile;
    cv::VideoWriter VideoWriterCap;
    cv::VideoWriter VideoWriterImage;
    cv::Mat CaptureImag;
    bool IsRunning;
    bool Recording;
    bool IsLiveVideo;
    bool IsCameraVideo;
    bool processingvideo;
    QString FileVideoCap;

signals:
    void SendCapturedImage(cv::Mat &);

public slots:
    void SendRecording(bool rec);
    void CaptureVideoFile(QString filenamevideo);
    void RecordImageVideo(cv::Mat &);

};

#endif // CAPTUREVID_H
