#include "jsonfiles.h"

Jsonfiles::Jsonfiles()
{
}
QString Jsonfiles::ReadJsonFile(QString field1,QString field2, QString field3,QString field4)
{
    QString jdata;

    JsonFile.open(QIODevice::ReadOnly | QIODevice::Text);
    JsonString = JsonFile.readAll();
    JsonFile.close();

    if(JsonString!="")
    {


        JsonDoc=QJsonDocument::fromJson(JsonString.toUtf8());

        JsonObj=JsonDoc.object();

        JsonArray=JsonObj.value(field1).toArray();

        foreach(const QJsonValue & value, JsonArray)
        {


            QJsonObject tempobj=value.toObject();

            if(field3=="")
                jdata=tempobj[field2].toString();
            else
            {

                JsonArray2=tempobj.value(field2).toArray();

                foreach(const QJsonValue & value2,JsonArray2)
                {
                    QJsonObject tempobj2=value2.toObject();
                    if(field4=="")
                        jdata=tempobj2[field3].toString();
                    else
                    {
                        JsonArray3=tempobj2.value(field3).toArray();
                        foreach(const QJsonValue & value3, JsonArray3)
                        {
                            QJsonObject tempobj3=value3.toObject();
                            jdata=tempobj3[field4].toString();
                        }
                    }
                }
            }
        }

    }
    else
   qDebug()<<"File doesn't exist";


    return jdata;

}

void Jsonfiles::WriteJsonFile(QString field1,QString field2, QString field3,QString field4, QString field5)
{

    JsonFile.open(QIODevice::ReadWrite | QIODevice::Text);
    JsonString = JsonFile.readAll();
    JsonFile.close();
   // qDebug()<<"FDT "<<val;

    JsonDoc = QJsonDocument::fromJson(JsonString.toUtf8());
    JsonObj = JsonDoc.object();

    JsonArray = JsonObj.value(field1).toArray();

    foreach (const QJsonValue & value, JsonArray)
          {
              QJsonObject obj = value.toObject();

              JsonArray2=obj.value(field2).toArray();

              if(field4=="")
              {
                  obj[field2]=QJsonValue(field3);

                  JsonArray.removeAt(0);
                  JsonArray.insert(0, obj);
                  JsonObj.insert(field1, JsonArray);
              }
              else
              {
              foreach (const QJsonValue & value2, JsonArray2)
                    {

                        QJsonObject obj2 = value2.toObject();

                        JsonArray3=obj2.value(field3).toArray();
                        if(field5=="")
                        {
                            obj2[field3]=QJsonValue(field4);
                            JsonArray2.removeAt(0);
                            JsonArray2.insert(0, obj2);
                            obj.insert(field2, JsonArray2);


                            JsonArray.removeAt(0);
                            JsonArray.insert(0, obj);
                            JsonObj.insert(field1, JsonArray);
                        }
                        else{


                        foreach (const QJsonValue & value3, JsonArray3)
                              {
                            QJsonObject obj3 = value3.toObject();
                            // qDebug()<<"type after"<<obj2;
                             obj3[field4]=QJsonValue(field5);
                            // obj["sensor2"]=obj2;

                             JsonArray3.removeAt(0);
                             JsonArray3.insert(0,obj3);
                             obj2.insert(field3,JsonArray3);



                             JsonArray2.removeAt(0);
                             JsonArray2.insert(0, obj2);
                             obj.insert(field2, JsonArray2);


                             JsonArray.removeAt(0);
                             JsonArray.insert(0, obj);
                             JsonObj.insert(field1, JsonArray);

                        }
                      }


                   }
              }

          }
        JsonDoc.setObject(JsonObj);

        JsonFile.setFileName(JsonFile.fileName());
        JsonFile.open(QIODevice::WriteOnly | QIODevice::Text);
        JsonFile.write(JsonDoc.toJson());
        JsonFile.close();

}

QString Jsonfiles::ReadJsonString(QString field1,QString field2, QString field3,QString field4)
{
    QString jdata;

      JsonDoc=QJsonDocument::fromJson(JsonString.toUtf8());

    if(!JsonDoc.isNull())
    {
        JsonObj=JsonDoc.object();
        JsonArray=JsonObj.value(field1).toArray();

        foreach(const QJsonValue & value, JsonArray)
        {
            QJsonObject tempobj=value.toObject();

            if(field3=="")
                jdata=tempobj[field2].toString();
            else
            {

                JsonArray2=tempobj.value(field2).toArray();

                foreach(const QJsonValue & value2,JsonArray2)
                {
                    QJsonObject tempobj2=value2.toObject();
                    if(field4=="")
                        jdata=tempobj2[field3].toString();
                    else
                    {
                        JsonArray3=tempobj2.value(field3).toArray();
                        foreach(const QJsonValue & value3, JsonArray3)
                        {
                            QJsonObject tempobj3=value3.toObject();
                            jdata=tempobj3[field4].toString();
                        }
                    }
                }
            }
        }

    }
    else
   qDebug()<<"String is not a JsonDocument";


    return jdata;

}


void Jsonfiles::WriteJsonString(QString field1,QString field2, QString field3,QString field4, QString *field5)
{
    unsigned int CounterFieldValues=0;
    JsonDoc = QJsonDocument::fromJson(JsonString.toUtf8());
    JsonObj = JsonDoc.object();

    JsonArray = JsonObj.value(field1).toArray();

    foreach (const QJsonValue & value, JsonArray)
          {
              QJsonObject obj = value.toObject();

              JsonArray2=obj.value(field2).toArray();

              if(field3=="")
              {
                  obj[field2]=QJsonValue(field5[CounterFieldValues]);

                  JsonArray.removeAt(CounterFieldValues);
                  JsonArray.insert(CounterFieldValues, obj);
                  JsonObj.insert(field1, JsonArray);
                  CounterFieldValues++;

              }
              else
              {
              foreach (const QJsonValue & value2, JsonArray2)
                    {

                        QJsonObject obj2 = value2.toObject();

                        JsonArray3=obj2.value(field3).toArray();
                        if(field4=="")
                        {
                            obj2[field3]=QJsonValue(field5[CounterFieldValues]);
                            CounterFieldValues++;
                            JsonArray2.removeAt(0);
                            JsonArray2.insert(0, obj2);
                            obj.insert(field2, JsonArray2);


                            JsonArray.removeAt(0);
                            JsonArray.insert(0, obj);
                            JsonObj.insert(field1, JsonArray);
                        }
                        else{


                        foreach (const QJsonValue & value3, JsonArray3)
                              {
                            QJsonObject obj3 = value3.toObject();
                            // qDebug()<<"type after"<<obj2;
                             obj3[field4]=QJsonValue(field5[CounterFieldValues]);
                            // obj["sensor2"]=obj2;

                             JsonArray3.removeAt(0);
                             JsonArray3.insert(0,obj3);
                             obj2.insert(field3,JsonArray3);



                             JsonArray2.removeAt(0);
                             JsonArray2.insert(0, obj2);
                             obj.insert(field2, JsonArray2);


                             JsonArray.removeAt(0);
                             JsonArray.insert(0, obj);
                             JsonObj.insert(field1, JsonArray);

                        }
                      }


                   }
              }

          }
        JsonDoc.setObject(JsonObj);
        JsonString=JsonDoc.toJson();
}
