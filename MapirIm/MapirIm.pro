#-------------------------------------------------
#
# Project created by QtCreator 2016-07-01T07:38:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MapirIm
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    nir.cpp \
    capturevid.cpp \
    jsonfiles.cpp

HEADERS  += mainwindow.h \
    nir.h \
    capturevid.h \
    jsonfiles.h

FORMS    += mainwindow.ui
INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib
LIBS+=-lopencv_videoio -lopencv_shape -lopencv_imgcodecs
LIBS+=-lopencv_core -lopencv_highgui -lopencv_imgproc #-lopencv_contrib
LIBS+=-lopencv_calib3d -lopencv_features2d -lopencv_flann -lopencv_ml
LIBS+=-lopencv_objdetect -lopencv_photo -lopencv_stitching -lopencv_superres
LIBS+=-lopencv_video -lopencv_videostab
