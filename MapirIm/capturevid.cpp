#include "capturevid.h"

CaptureVid::CaptureVid(QObject *parent) :
    QThread(parent)
{


}

void CaptureVid::run()
{
    qDebug()<<"run";

    if(IsLiveVideo)
    {
       CaptureVideo.open(0);
       VideoWriterCap=cv::VideoWriter("out.avi",cv::VideoWriter::fourcc('M','J','P','G'),20, Size(CaptureVideo.get(CAP_PROP_FRAME_WIDTH),CaptureVideo.get(CAP_PROP_FRAME_HEIGHT)),true);
       VideoWriterImage=cv::VideoWriter("VideoProccess.avi",cv::VideoWriter::fourcc('M','J','P','G'),20, Size(CaptureVideo.get(CAP_PROP_FRAME_WIDTH),CaptureVideo.get(CAP_PROP_FRAME_HEIGHT)),true);
    }
    if(IsCameraVideo)
    {
       CapVideoFile.open(FileVideoCap.toStdString().c_str());
       qDebug()<<"cap open"<<CapVideoFile.get(CAP_PROP_FRAME_WIDTH)<<CapVideoFile.get(CAP_PROP_FRAME_HEIGHT)<<CapVideoFile.get(CAP_PROP_FPS);
       VideoWriterImage=cv::VideoWriter("VideoProccess.avi",cv::VideoWriter::fourcc('m','p','4','v'),CapVideoFile.get(CAP_PROP_FPS), Size(640,480),true);
    }

    while(1)
    {
        if(IsLiveVideo)
        {
            if(CaptureVideo.isOpened())
            {
            CaptureVideo.read(CaptureImag);
            emit SendCapturedImage(CaptureImag);
             if(Recording&&!CaptureImag.empty())
             {
             VideoWriterCap.write(CaptureImag);
             }
            }
        }
        else break;
    }

    cv::Mat frame;
    while(1)
    {
        if(IsCameraVideo)
        {

          if(CapVideoFile.isOpened())
           {
             CapVideoFile.read(frame);
             if(!frame.empty())
              {
                emit SendCapturedImage(frame);
                //this->msleep(50);
                 while(processingvideo);
               }

           }
        }
        else break;
    }
    cv::Mat ZerosImage=cv::Mat::zeros(240,320,CV_8UC3);
    emit SendCapturedImage(ZerosImage);

qDebug()<<"release";
}

void CaptureVid::stop()
{

    VideoWriterCap.release();
    VideoWriterImage.release();

    if(IsLiveVideo)
    IsLiveVideo=false;
    if(IsCameraVideo)
    IsCameraVideo=false;
}



void CaptureVid::SendRecording(bool rec)
{
    Recording=rec;
}

void CaptureVid::CaptureVideoFile(QString filenamevideo)
{
    FileVideoCap=filenamevideo;
}

void CaptureVid::RecordImageVideo(cv::Mat &imagetorecord)
{
    if(Recording)
    {
        if(!imagetorecord.empty())
        {
            VideoWriterImage.write(imagetorecord);
        }

    }
}
