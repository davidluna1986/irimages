#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>

#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include<nir.h>
#include<capturevid.h>
#include<jsonfiles.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void connections();
    void LoadConfigJsonFile();

signals:
    void SendRecording(bool);
    void CaptureVideoFile(QString);
    void RecordImageVideo(cv::Mat &);
public slots:
    void GetCapturedImage(cv::Mat &);
private slots:
    void on_pushButtonSaveParam_clicked();

    void on_pushButtonLive_clicked();

    void on_pushButtonLoadNir_clicked();

    void on_comboBoxTypeImage_currentIndexChanged(const QString &arg1);

    void on_pushButtonSaveImage_clicked();

    void on_checkBoxRecord_clicked(bool checked);

    void on_pushButtonLoadVideo_clicked();

    void on_pushButtonStopVideo_clicked();

    void on_radioButtonlive_clicked(bool checked);

    void on_radioButtonVideo_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    NIR *NirObj;
    CaptureVid *CaptureVideo;
    Jsonfiles *JConfigFile;
    bool CaptureLive;
};

#endif // MAINWINDOW_H
