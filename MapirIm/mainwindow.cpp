#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;
using namespace cv;

Q_DECLARE_METATYPE(cv::Mat)


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    qRegisterMetaType< cv::Mat>("cv::Mat&");

    NirObj = new NIR(this);
    CaptureVideo=new CaptureVid(this);
    JConfigFile=new Jsonfiles();

    NirObj->FileNameLUT="/home/david/QTPrograms/Basics/MAPIR/DataIn/MAPIR_ndvi_0-1.bmp";
    NirObj->imLUT = cv::imread(NirObj->FileNameLUT.toStdString());

    LoadConfigJsonFile();

    connections();
    CaptureLive=false;
    ui->comboBoxTypeImage->setCurrentText("NDVI_COLOR");
    CaptureVideo->IsLiveVideo=true;
    ui->pushButtonLoadVideo->setEnabled(false);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connections()
{
    this->connect(CaptureVideo,SIGNAL(SendCapturedImage(cv::Mat &)),this,SLOT(GetCapturedImage(cv::Mat &)));
    this->connect(this,SIGNAL(SendRecording(bool)),CaptureVideo,SLOT(SendRecording(bool)));
    this->connect(this,SIGNAL(CaptureVideoFile(QString)),CaptureVideo,SLOT(CaptureVideoFile(QString)));
    this->connect(this,SIGNAL(RecordImageVideo(cv::Mat &)),CaptureVideo,SLOT(RecordImageVideo(cv::Mat &)));
}

void MainWindow::LoadConfigJsonFile()
{
    QString TempString;
    JConfigFile->JsonFile.setFileName("/home/david/QTPrograms/Basics/MapirIm/JConfiguration");

    TempString=JConfigFile->ReadJsonFile("config","slopevis","","");
    if(TempString!="")
    {
       NirObj->slopeVis=TempString.toDouble();
       ui->doubleSpinBoxSLopeVis->setValue(NirObj->slopeVis);
    }

    TempString=JConfigFile->ReadJsonFile("config","offsetvis","","");
    if(TempString!="")
    {
        NirObj->intercVis=TempString.toDouble();
        ui->doubleSpinBoxOffsetVis->setValue(NirObj->intercVis);
    }
    TempString=JConfigFile->ReadJsonFile("config","slopenir","","");
    if(TempString!="")
    {
        NirObj->slopeNIR=TempString.toDouble();
        ui->doubleSpinBoxSlopeNIR->setValue(NirObj->slopeNIR);
    }
    TempString=JConfigFile->ReadJsonFile("config","offsetnir","","");
    if(TempString!="")
    {
        NirObj->intercNIR=TempString.toDouble();
        ui->doubleSpinBoxOffsetNIR->setValue(NirObj->intercNIR);
    }
}

void MainWindow::GetCapturedImage(cv::Mat &ImageCaptured)
{
   // cv::imshow("Live",ImageCaptured);
    CaptureVideo->processingvideo=true;
    cv::Mat captureimg;
    ImageCaptured.copyTo(captureimg);
    NirObj->GetNDVIColor(captureimg,&NirObj->NDVIColorImage);
    NirObj->GetColorPlanes(captureimg,&NirObj->BlueImage,&NirObj->IRImage);
    //Preparacion de variables a operar
    //NirObj->CaptureImg = cv::imread(filenameLocate);
    //NirObj->CaptureVid.read(NirObj->CaptureImg);

//    cv::imshow("LUT",NirObj->imLUT);
  //  cv::imshow("Original", captureimg);
  //  cv::imshow("VIS/Blue", NirObj->BlueImage);
  //  cv::imshow("NIR/Red", NirObj->IRImage);
//    cv::imshow("NDVI_disp", outNDVI_disp);

    cv::cvtColor(captureimg,captureimg,cv::COLOR_BGR2RGB);
    QImage QtColorImage( captureimg.data, captureimg.cols, captureimg.rows, captureimg.step, QImage::Format_RGB888 );
    QPixmap QtColorPixmap=QPixmap::fromImage(QtColorImage, Qt::AutoColor);
    ui->labelNIRImage->setPixmap(QtColorPixmap.scaled(320,240,Qt::KeepAspectRatio));

  //  cv::imshow("NDVI_color", NirObj->NDVIColorImage);
    emit RecordImageVideo(NirObj->NDVIColorImage);
    on_comboBoxTypeImage_currentIndexChanged(ui->comboBoxTypeImage->currentText());

    CaptureVideo->processingvideo=false;
}



void MainWindow::on_pushButtonSaveParam_clicked()
{

   QMessageBox::StandardButton reply;
   reply=QMessageBox::question(this,"Valve","Save New Configuration?",QMessageBox::Yes|QMessageBox::No);
   if(reply == QMessageBox::Yes)
   {
       JConfigFile->JsonFile.setFileName("/home/david/QTPrograms/Basics/MapirIm/JConfiguration");

       JConfigFile->WriteJsonFile("config","slopevis",QString::number(ui->doubleSpinBoxSLopeVis->value(),'d',8),"","");
       JConfigFile->WriteJsonFile("config","offsetvis",QString::number(ui->doubleSpinBoxOffsetVis->value(),'d',8),"","");
       JConfigFile->WriteJsonFile("config","slopenir",QString::number(ui->doubleSpinBoxSlopeNIR->value(),'d',8),"","");
       JConfigFile->WriteJsonFile("config","offsetnir",QString::number(ui->doubleSpinBoxOffsetNIR->value(),'d',8),"","");


       /****************DEFAULT VALUES****************/
       //    {
       //        "config": [
       //            {
       //                "offsetnir": "0.00009811",
       //                "offsetvis": "-0.17479136",
       //                "slopenir": "2.36780164",
       //                "slopevis": "0.81548662"
       //            }
       //        ]
       //    }
       /**********************************************/
   }

}

void MainWindow::on_pushButtonLive_clicked()
{
    if(!CaptureLive)
    {
     CaptureLive=true;
     CaptureVideo->IsLiveVideo=ui->radioButtonlive->isChecked();
     CaptureVideo->IsCameraVideo=ui->radioButtonVideo->isChecked();
     CaptureVideo->start();
     ui->pushButtonLive->setText("Stop");
    }
    else
    {
     CaptureLive=false;
     CaptureVideo->stop();
     ui->pushButtonLive->setText("Play");
    }
}

void MainWindow::on_pushButtonStopVideo_clicked()
{

}

void MainWindow::on_pushButtonLoadNir_clicked()
{

    CaptureLive=false;
    CaptureVideo->stop();

    QStringList pathselected=QFileDialog::getOpenFileNames(this, "Select a file to open...", QDir::homePath(),"*.jpg");

    if(pathselected.length()>0)
    {

       NirObj->LoadedImage=cv::imread(pathselected[0].toStdString());
       ui->lineEditPathImage->setText(pathselected[0]);
       if(!NirObj->LoadedImage.empty())
       GetCapturedImage(NirObj->LoadedImage);
    }
}

void MainWindow::on_comboBoxTypeImage_currentIndexChanged(const QString &arg1)
{
    if(arg1=="NDVI_COLOR")
    {
        if(!NirObj->NDVIColorImage.empty())
        {
            cv::cvtColor(NirObj->NDVIColorImage,NirObj->NDVIColorImage,cv::COLOR_BGR2RGB);
            QImage QtNvdiColorImage( NirObj->NDVIColorImage.data, NirObj->NDVIColorImage.cols, NirObj->NDVIColorImage.rows, NirObj->NDVIColorImage.step, QImage::Format_RGB888 );
            QPixmap QtNvdiColorPixmap=QPixmap::fromImage(QtNvdiColorImage, Qt::AutoColor);
            ui->labelOtherImage->setPixmap(QtNvdiColorPixmap.scaled(320,240,Qt::KeepAspectRatio));
        }
    }
    if(arg1=="BLUE/VIS")
    {
        if(!NirObj->BlueImage.empty())
        {
            QImage QtBlueColorImage( NirObj->BlueImage.data, NirObj->BlueImage.cols, NirObj->BlueImage.rows, NirObj->BlueImage.step, QImage::Format_Indexed8 );
            QPixmap QtBlueColorPixmap=QPixmap::fromImage(QtBlueColorImage, Qt::AutoColor);
            ui->labelOtherImage->setPixmap(QtBlueColorPixmap.scaled(320,240,Qt::KeepAspectRatio));
        }
    }
    if(arg1=="RED/IR")
    {
        if(!NirObj->IRImage.empty())
        {
            QImage QtIrColorImage( NirObj->IRImage.data, NirObj->IRImage.cols, NirObj->IRImage.rows, NirObj->IRImage.step, QImage::Format_Indexed8 );
            QPixmap QtIrColorPixmap=QPixmap::fromImage(QtIrColorImage, Qt::AutoColor);
            ui->labelOtherImage->setPixmap(QtIrColorPixmap.scaled(320,240,Qt::KeepAspectRatio));
        }
    }
}

void MainWindow::on_pushButtonSaveImage_clicked()
{
    QString pathselected=QFileDialog::getSaveFileName(this, "Select a file to open...", QDir::homePath(),".jpg");

    if(pathselected.length()>0)
    {
       if(!NirObj->NDVIColorImage.empty())
       {
           cv::cvtColor(NirObj->NDVIColorImage,NirObj->NDVIColorImage,cv::COLOR_BGR2RGB);
           imwrite((pathselected+"NDVI"+".jpg").toStdString(), NirObj->NDVIColorImage);
       }

       if(!NirObj->IRImage.empty())
       {
           imwrite((pathselected+"IR"+".jpg").toStdString(), NirObj->IRImage);
       }

       if(!NirObj->BlueImage.empty())
       {
           imwrite((pathselected+"BLUE"+".jpg").toStdString(), NirObj->BlueImage);
       }
    }


}

void MainWindow::on_checkBoxRecord_clicked(bool checked)
{
    emit SendRecording(checked);
}

void MainWindow::on_pushButtonLoadVideo_clicked()
{
    QStringList pathselected=QFileDialog::getOpenFileNames(this, "Select a file to open...", QDir::homePath());

    if(pathselected.length()>0)
    {
        emit CaptureVideoFile(pathselected[0]);
    }

}


void MainWindow::on_radioButtonlive_clicked(bool checked)
{
      CaptureVideo->IsLiveVideo=checked;
}

void MainWindow::on_radioButtonVideo_clicked(bool checked)
{
      CaptureVideo->IsCameraVideo=checked;
      ui->pushButtonLoadVideo->setEnabled(checked);
}
