#ifndef JSONFILES_H
#define JSONFILES_H

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QDebug>
#include <QFile>

class Jsonfiles
{
public:
    Jsonfiles();
    QString ReadJsonFile(QString field1,QString field2, QString field3,QString field4);
    QString ReadJsonString(QString field1,QString field2, QString field3,QString field4);
    void WriteJsonFile(QString field1,QString field2, QString field3,QString field4,QString field5);
    void WriteJsonString(QString field1, QString field2, QString field3, QString field4, QString *field5);

    QJsonObject JsonObj;
    QJsonDocument JsonDoc;
    QString JsonString;
    QString ValueString;
    QJsonValue JsonValue;
    QJsonArray JsonArray;
    QJsonArray JsonArray2;
    QJsonArray JsonArray3;
    QFile JsonFile;

};

#endif // JSONFILES_H
