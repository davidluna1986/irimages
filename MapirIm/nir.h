#ifndef NIR_H
#define NIR_H

#include <QObject>
#include <QDebug>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace cv;

class NIR : public QObject
{
    Q_OBJECT
public:
    explicit NIR(QObject *parent = 0);

    void NDVI(cv::Mat srcVis, cv::Mat srcNIR, cv::Mat *out, short option);
    void scale_image(cv::Mat src, cv::Mat *dst, short option, short mode);
    void scale_image_byte(cv::Mat src, cv::Mat *dst, short mode);
    void GetNDVIColor(cv::Mat src,cv::Mat *dst);
    void GetColorPlanes(cv::Mat src, cv::Mat *out1,cv::Mat *out2);

    cv::VideoCapture CaptureVid;
    cv::Mat CaptureImg;
    cv::Mat LoadedImage;
    cv::Mat NDVIColorImage;
    cv::Mat BlueImage;
    cv::Mat IRImage;
    cv::Mat imLUT;
    QString FileNameLUT;

    double slopeVis;
    double slopeNIR;
    double intercVis;
    double intercNIR;

signals:

public slots:

};

#endif // NIR_H
